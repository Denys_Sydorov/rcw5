import React, { useContext, useState, useEffect } from 'react';
import Service from '../../services/service';
import CircularProgress from '@material-ui/core/CircularProgress';


import './random-planet.css';

const RandomPlanet = () => {
  const [planets, setPlanets] = useState(null);
  let random = Math.floor((Math.random() * 9) + 1);
  console.log(random)

  useEffect(() => {
    new Service().getPlanets().then((data) => {
      setTimeout(() => {
        setPlanets(data.results)
      }, 2000)
    })
  }, [planets]);


  console.dir(planets)

  return planets === null ? < CircularProgress /> :

    <div className="random-planet jumbotron rounded">
      <img className="planet-image"
        /* src="https://starwars-visualguide.com/assets/img/planets/5.jpg" */
        src={`https://starwars-visualguide.com/assets/img/planets/${random + 1}.jpg`} />

      <div>
        <h4>{planets[random].name}</h4>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">
            <span className="term">Population</span>
            <span>{planets[random].population}</span>
          </li>
          <li className="list-group-item">
            <span className="term">Rotation Period</span>
            <span>{planets[random].rotation_period}</span>
          </li>
          <li className="list-group-item">
            <span className="term">Diameter</span>
            <span>{planets[random].diameter}</span>
          </li>
        </ul>
      </div>
    </div>
}



export default RandomPlanet;



/*   const planets = new Service().getPlanets()
  const id = Math.floor(Math.random() * 9) + 1
  console.log(id)
  const [randomInformation, setRandomInformation] = useState([])
  useEffect(() => {
    planets.then(data => {
      setTimeout(() => {

        setRandomInformation(data.results[id])
      }, 2000)

    })
  }, []) */
